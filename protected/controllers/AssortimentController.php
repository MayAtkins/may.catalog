<?php

class AssortimentController extends Controller
{
        /**
         * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
         * using two-column layout. See 'protected/views/layouts/column2.php'.
         */
        public $layout='//layouts/column2';
        //public $defaultAction='view';

        /**
         * @return array action filters
         */
        public function filters()
        {
                return array(
                        'accessControl', // perform access control for CRUD operations
                );
        }

        /**
         * Specifies the access control rules.
         * This method is used by the 'accessControl' filter.
         * @return array access control rules
         */
        public function accessRules()
        {
                return array(
                        array('allow',  // allow all users to perform 'index' and 'view' actions
                                'actions'=>array('index','view'),
                                'users'=>array('*'),
                        ),
                        array('allow', // allow authenticated user to perform 'create' and 'update' actions
                                'actions'=>array('create','update'),
                                'users'=>array('@'),
                        ),
                        array('allow', // allow admin user to perform 'admin' and 'delete' actions
                                'actions'=>array('admin','delete'),
                                'users'=>array('admin'),
                        ),
                        array('deny',  // deny all users
                                'users'=>array('*'),
                        ),
                );
        }

        /**
         * Displays a particular model.
         * @param integer $id the ID of the model to be displayed
         */
        public function actionView($id)
        {
                $this->render('view',array(
                        'model'=>$this->loadModel($id),
                    
                ));
        }

        /**
         * Creates a new model.
         * If creation is successful, the browser will be redirected to the 'view' page.
         */
        public function actionCreate()
{
                    $model=new Assortiment;
            
// Uncomment the following line if AJAX validation is needed
// $this->performAjaxValidation($model);
            if(isset($_POST['Assortiment'])){
                    $model->attributes=$_POST['Assortiment'];
//Полю icon присвоить значения поля формы icon
                    $model->icon=CUploadedFile::getInstance($model,'icon');

            if($model->save()){
//Если поле загрузки файла не было пустым, то            
             if ($model->icon){
//сохранить файл на сервере в каталог assortiment_img под именем 
//id_assortiment.jpg
                $file = Yii::app()->basePath.'/../assortiment_img/'.$model->id.'_assortiment.jpg';
                $model->icon->saveAs($file);
                                }
            $this->redirect(array('view','id'=>$model->id)); 
                }
        }
        $this->render('create',array(
    'model'=>$model,
        ));
}

        /**
         * Updates a particular model.
         * If update is successful, the browser will be redirected to the 'view' page.
         * @param integer $id the ID of the model to be updated
         */
        public function actionUpdate($id)
{
            $model=$this->loadModel($id);
            if(isset($_POST['Assortiment'])){
                $model->attributes=$_POST['Assortiment'];
  
                $model->icon=CUploadedFile::getInstance($model,'icon');
        if($model->save()){
//Если отмечен чекбокс «удалить файл»            
            if($model->del_img)
{
       if(file_exists(Yii::app()->basePath.'/../assortiment_img/'.$id.'_assortiment.jpg'))
            {
//удаляем файл
            unlink('./assortiment_img/'.$id.'_assortiment.jpg');
        }
        }
//Если поле загрузки файла не было пустым, то            
            if ($model->icon){
            $file = Yii::app()->basePath.'/../assortiment_img/'.$model->id.'_assortiment.jpg';
//сохранить файл на сервере в каталог assortiment_img под именем 
//id_assortiment.jpg Если файл с таким именем существует, он будет заменен.
            $model->icon->saveAs($file);
}
            $this->redirect(array('view','id'=>$model->id)); 
            }
        }
$this->render('update',array(
'model'=>$model,
));
}

        /**
         * Deletes a particular model.
         * If deletion is successful, the browser will be redirected to the 'admin' page.
         * @param integer $id the ID of the model to be deleted
         */
        public function actionDelete($id)
        {
                if(Yii::app()->request->isPostRequest)
                {
                        // we only allow deletion via POST request
                        $this->loadModel($id)->delete();

                        // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
                        if(!isset($_GET['ajax']))
                                $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
                }
                else
                        throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
        }

        /**
         * Lists all models.
         */
        public function actionIndex()
            {
        if(!empty($_GET['category']))
//Пришла переменная category, то есть пользователь 
//выбрал какую-то категорию из меню
                {   
                    $criteria=new CDbCriteria(array(
                    'condition'=>
                    'category_id=:id and
                    t.shown=1 and
                    category.shown=1',
                    'params' => array(':id'=>(int)$_GET['category']),
                    'with' => array('category'),
                        ));
                }         
    else
        { //Пользователь ничего не выбрал из меню
            $criteria=new CDbCriteria(array(
            'condition'=>'t.shown=1 and category.shown=1',
            'with' => array('category'),                              
            ));   
        }
               
        $dataProvider=new CActiveDataProvider('Assortiment',array(
        'criteria'=>$criteria));
$this->render('index',array(
        'dataProvider'=>$dataProvider,
                ));
    }

        /**
         * Manages all models.
         */
        public function actionAdmin()
        {
                $model=new Assortiment('search');
                $model->unsetAttributes();  // clear any default values
                if(isset($_GET['assortiment']))
                        $model->attributes=$_GET['assortiment'];

                $this->render('admin',array(
                        'model'=>$model,
                ));
        }

        /**
         * Returns the data model based on the primary key given in the GET variable.
         * If the data model is not found, an HTTP exception will be raised.
         * @param integer the ID of the model to be loaded
         */
        public function loadModel($id)
        {
                $model=Assortiment::model()->findByPk($id);
                if($model===null)
                        throw new CHttpException(404,'The requested page does not exist.');
                return $model;
        }

        /**
         * Performs the AJAX validation.
         * @param CModel the model to be validated
         */
        protected function performAjaxValidation($model)
        {
                if(isset($_POST['ajax']) && $_POST['ajax']==='assortiment-form')
                {
                        echo CActiveForm::validate($model);
                        Yii::app()->end();
                }
        }
    public function findCategory()
{ 
            $model = Category::model()->findByPk((int)$_GET['category']); 
                        return $model; 

} 
public function assortiment_image($id,$name, $width='200', 
            $class='assortiment_img')
{
        if(file_exists(Yii::app()->basePath.'/../assortiment_img/'.$id.'_assortiment.jpg'))
     return CHtml::image(Yii::app()->urlManager->baseUrl.'/../assortiment_img/'.$id.'_assortiment.jpg',$name,
                    array(
                        'width'=>$width,
                        'class'=>$class,
        ));
    else
     return CHtml::image(Yii::app()->urlManager->baseUrl.'/../assortiment_img/no_photo.jpg','No photo',
                    array(
                        'width'=>$width,
                        'class'=>$class
        ));
}
}
