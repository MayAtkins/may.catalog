<?php

/**
 * This is the model class for table "{{sppage}}".
 *
 * The followings are the available columns in table '{{sppage}}':
 * @property integer $id
 * @property string $name
 * @property string $url
 *
 * The followings are the available model relations:
 * @property Spmessage[] $tblSpmessages
 */
class Sppage extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Sppage the static model class
	 */
    
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{sppage}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('name', 'length', 'max'=>45),
			array('url', 'length', 'max'=>20),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, name, url', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
{
// NOTE: you may need to adjust the relation name and the related
// class name for the relations automatically generated below.
                        return array(
            'messages' => array(self::MANY_MANY, 'Spmessage', 
            'tbl_sprelations(tbl_sppage_id, tbl_spmessage_id)',
            'order'=>'messages_messages.sort ASC'));
   }

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'name' => 'Name',
			'url' => 'Url',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('url',$this->url,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
        /**
* Returns all staticpages.
*/
// Буду генерировать массив пунктов меню $r. 
            public function Staticpages_menu()
        {
//Первый пункт меню: Каталог. 
        $r[] =   array('label'=>'Catalog', 'url'=>array('/assortiment/index'), 
            'active' => Yii::app()->controller->getId() == 'assortiment');
//Далее беру все записи из модели  sppage отсортированные по полю 'sorting' 
//  и имеющие атрибут 'shown'=yes
                $models = sppage::model()->findAll(
                        array(
                    'condition' => 'shown = "yes"',
            )
            );
//Прохожусь по всем строкам и генерую на их основе меню (элементы массива $r)
            foreach($models as $val)
    {
$r[] =   array(
'label'=>$val->name, 
'url'=>array('/page='.$val->url),
'active' => ($val->url == preg_replace("|[^a-z0-9]|i", NULL,$_GET['id'])),
);
    }
//В конец меню добавляю пункт Контакт, логин, логофф.
        $r[] =	array('label'=>'Contact', 'url'=>array('/site/contact'));
        $r[] =	array('label'=>'Login', 'url'=>array('/site/login'), 
                'visible'=>Yii::app()->user->isGuest);
        $r[] =	array('label'=>'Logout', 'url'=>array('/site/logout'), 
        'visible'=>!Yii::app()->user->isGuest);    				

//Данный метод возвращает массив пунктов меню
return $r;
        }
}