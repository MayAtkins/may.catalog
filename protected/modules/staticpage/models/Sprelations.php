<?php

/**
 * This is the model class for table "{{sprelations}}".
 *
 * The followings are the available columns in table '{{sprelations}}':
 * @property integer $tbl_sppage_id
 * @property integer $tbl_spmessage_id
 */
class Sprelations extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Sprelations the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{sprelations}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('tbl_sppage_id, tbl_spmessage_id', 'required'),
			array('tbl_sppage_id, tbl_spmessage_id', 'numerical', 'integerOnly'=>true),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('tbl_sppage_id, tbl_spmessage_id', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
{
// NOTE: you may need to adjust the relation name and the related
// class name for the relations automatically generated below.
        return array(
            'message' => array(self::BELONGS_TO, 'Spmessage', 'tbl_spmessage_id'),
            'page' => array(self::BELONGS_TO, 'Sppage', 'tbl_sppage_id'),
        );
}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'tbl_sppage_id' => 'Tbl Sppage',
			'tbl_spmessage_id' => 'Tbl Spmessage',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('tbl_sppage_id',$this->tbl_sppage_id);
		$criteria->compare('tbl_spmessage_id',$this->tbl_spmessage_id);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}