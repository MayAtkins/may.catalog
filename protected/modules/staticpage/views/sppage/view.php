<?php
$this->breadcrumbs=array(
	'Sppages'=>array('index'),
	$model->name,
);

$this->menu=array(
	array('label'=>'List Sppage', 'url'=>array('index')),
	array('label'=>'Create Sppage', 'url'=>array('create')),
	array('label'=>'Update Sppage', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete Sppage', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Sppage', 'url'=>array('admin')),
);
?>

<h1><?php echo $model->name; ?></h1>

<?php foreach($model->messages as $item)
{
  echo '<H2>'.$item->title.'</H2>';
  echo $item->message;
  echo '<br><br>';
}  
 ?>
