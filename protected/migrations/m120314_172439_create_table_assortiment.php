<?php

class m120314_172439_create_table_assortiment extends CDbMigration
{
	 public function up()
        {
                $this->createTable('tbl_assortiment', array(
                        'id' => 'pk',
                        'name' => 'text',
                        'description' => 'text',
                        'category_id' => 'int',
                        'rating' => 'int',
                        'shown' => 'boolean NOT NULL DEFAULT 1',
                ),"ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci");
                $this->addForeignKey('category_id', 'tbl_assortiment', 'category_id',
                                     'tbl_category', 'id', 'CASCADE', 'CASCADE');
        }

         public function down()
        {
                $this->dropTable('tbl_assortiment');
        }
	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}