<?php

class m120318_185742_create_table_sppage extends CDbMigration
{
	public function up()
	{
            $this->createTable('tbl_sppage', array(
                'id' => 'pk',
                'name' => 'VARCHAR(45)',
                'url' => 'VARCHAR(20)',
            ),"ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci");
	}

	public function down()
	{
		$this->dropTable('tbl_sppage');
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}