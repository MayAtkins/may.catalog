<?php

class m120318_185831_create_table_spmessage extends CDbMigration
{
	public function up()
	{
            $this->createTable('tbl_spmessage', array(
                    'id' => 'INT',
                    'title' => 'TEXT',
                    'message' => 'TEXT',
                    ),"ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci");
	}

	public function down()
	{
		$this->dropTable('tbl_spmessage');
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}