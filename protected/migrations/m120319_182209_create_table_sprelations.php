<?php

class m120319_182209_create_table_sprelations extends CDbMigration
{
	



	public function up()
    {
            $this->createTable('tbl_sprelations',array(
                'tbl_sppage_id'=> 'int not null',
                'tbl_spmessage_id'=> 'int not null',
            ));
            $this->addForeignKey('tbl_sppage_id', 'tbl_sprelations', 'tbl_sppage_id',
                                     'tbl_sppage', 'id');
            $this->addForeignKey('tbl_spmessage_id', 'tbl_sprelations', 'tbl_spmessage_id',
                                     'tbl_spmessage', 'id');
    }



	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}