<?php

/**
 * This is the model class for table "tbl_assortiment".
 *
 * The followings are the available columns in table 'tbl_assortiment':
 * @property integer $id
 * @property string $name
 * @property string $description
 * @property integer $category_id
 * @property integer $rating
 * @property integer $shown
 *
 * The followings are the available model relations:
 * @property TblCategory $category
 */
class Assortiment extends CActiveRecord
{
    // assortiment_img value
public $icon;
// Delete picture boolean
public $del_img;
        /**
         * Returns the static model of the specified AR class.
         * @param string $className active record class name.
         * @return assortiment the static model class
         */
        public static function model($className=__CLASS__)
        {
                return parent::model($className);
        }

        /**
         * @return string the associated database table name
         */
        public function tableName()
        {
                return '{{assortiment}}';
        }

        /**
         * @return array validation rules for model attributes.
         */
        public function rules()
{
// NOTE: you should only define rules for those attributes that
// will receive user inputs.
                    return array(
                    array('name, description, category_id, rating', 'required'),
                    array('del_img', 'boolean'),
                    array('category_id, rating', 'numerical', 'integerOnly'=>true),
                    array('shown','safe'),
                    array('icon', 'file',
                        'types'=>'jpg, gif, png',
                        'maxSize'=>'5242880' , // 5MB
                        'allowEmpty'=>'true',
                    'tooLarge'=>'The file was larger than 5MB. Please upload a smaller file.',
            ),
// The following rule is used by search().
// Please remove those attributes that should not be searched.
                    array(
'id, name, description, category_id, rating, shown', 'safe', 'on'=>'search'),
);
}

        /**
         * @return array relational rules.
         */
        public function relations()
        {
                // NOTE: you may need to adjust the relation name and the related
                // class name for the relations automatically generated below.
                return array(
                        'category' => array(self::BELONGS_TO, 'Category', 'category_id'),
                );
        }

        /**
         * @return array customized attribute labels (name=>label)
         */
        public function attributeLabels()
            {
                return array(
                'id' => 'Id',
                'name' => 'Name',
                'description' => 'Description',
                'category_id' => 'Category',
                'rating' => 'Rating',
                'shown' => 'Show in catalog?',
                'icon' => 'Image',
                'del_img'=>'Delete image?',
);
}

        /**
         * Retrieves a list of models based on the current search/filter conditions.
         * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
         */
        public function search()
        {
                // Warning: Please modify the following code to remove attributes that
                // should not be searched.

                $criteria=new CDbCriteria;

                $criteria->compare('id',$this->id);
                $criteria->compare('name',$this->name,true);
                $criteria->compare('description',$this->description,true);
                $criteria->compare('category_id',$this->category_id);
                $criteria->compare('rating',$this->rating);
                $criteria->compare('shown',$this->shown);

                return new CActiveDataProvider($this, array(
                        'criteria'=>$criteria,
                ));
        }
}