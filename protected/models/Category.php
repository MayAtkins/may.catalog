<?php

/**
 * This is the model class for table "tbl_category".
 *
 * The followings are the available columns in table 'tbl_category':
 * @property integer $id
 * @property string $name
 * @property string $description
 * @property integer $shown
 *
 * The followings are the available model relations:
 * @property TblAssortiment[] $tblAssortiments
 */
class Category extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Category the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{category}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('shown', 'numerical', 'integerOnly'=>true),
			array('name, description', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, name, description, shown', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'Assortiments' => array(self::HAS_MANY, 'Assortiment', 'category_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'name' => 'Name',
			'description' => 'Description',
			'shown' => 'Shown',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('description',$this->description,true);
		$criteria->compare('shown',$this->shown);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
        public static function All_shown()
{

                  $r[] =   array('label'=>'Show all', 'url'=>array('/assortiment'), 
                            'active' => ((Yii::app()->controller->getId() == 
                            'assortiment')and(!Yii::app()->request->getParam('category')) and 
                        (!Yii::app()->request->getParam('id'))));      
                        $models = Category::model()->findAll(
                           array('order' => 'name',
                                'condition' => 'shown = "1"',
            )
          );
        foreach($models as $val)
{
            
            $model = Assortiment::model()->findByPk((int)!Yii::app()->request->getParam('id')); 
            if ($model) {
             $r[] =   array('label'=>$val->name, 
            'url'=>array('/assortiment/index','category'=>$val->id), 
                'active' => (($val->id == $model->category_id)or
                ($val->id == (int)!Yii::app()->request->getParam('category'))));
            }else
            {
                 $r[] =   array('label'=>$val->name, 
            'url'=>array('/assortiment/index','category'=>$val->id));
            }
}

return $r;
}
        /**
* Returns the list of all category.
* @return list data of all category
*/
                public static function All()
        {
                        $models = Category::model()->findAll(
                                array('order' => 'name'));
                            $list = CHtml::listData($models,'id', 'name', 'shown');
                                return $list;
}
}