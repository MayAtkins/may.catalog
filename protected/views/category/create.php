<?php
$this->breadcrumbs=array(
	'Categories'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'Лист категори', 'url'=>array('index')),
	array('label'=>'Управление категории', 'url'=>array('admin')),
);
?>

<h1>Создание катигорией</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>