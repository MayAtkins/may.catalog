    <?php echo '<a name="'.$data->id.'"></a>'; ?>
<div class="view">
            <b><?php echo CHtml::encode($data->getAttributeLabel('name')); ?>:</b>
     <?php echo CHtml::link(CHtml::encode($data->name), array('view','id'=>$data->id)); ?>
    <br /> 
     <b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
    <?php echo CHtml::encode($data->id); ?>
    <br />
    <b><?php echo CHtml::encode($data->getAttributeLabel('category_id')); ?>:</b>
    <?php echo CHtml::encode($data->category->name); ?>
    <br />
    <?php echo $this->assortiment_image($data->id, $data->name) ; ?>
    <br />
    <b><?php echo CHtml::encode($data->getAttributeLabel('description')); ?>:</b>
    <?php echo $data->description; ?>
<br />
</div>