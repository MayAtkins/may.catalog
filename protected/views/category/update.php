<?php
$this->breadcrumbs=array(
	'Categories'=>array('index'),
	$model->name=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'Лист категории', 'url'=>array('index')),
	array('label'=>'Создание категории', 'url'=>array('create')),
	array('label'=>'Смотреть категории', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Управление категориии', 'url'=>array('admin')),
);
?>

<h1>Изменение категории <?php echo $model->id; ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>