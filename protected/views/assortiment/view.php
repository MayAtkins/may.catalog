<?php
$this->breadcrumbs=array(
        'Assortiments'=>array('index'),
        $model->name,
);

$this->menu=array(
        array('label'=>'Лист ассортиментов', 'url'=>array('index')),
        array('label'=>'Создать ассортимент', 'url'=>array('create')),
        array('label'=>'Изменить ассортимент', 'url'=>array('update', 'id'=>$model->id)),
        array('label'=>'Удалить ассортимент', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
        array('label'=>'Управление ассортиментом', 'url'=>array('admin')),
);
?>

<h1>Смотреть Ассортименты #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
            'data'=>$model,
            'attributes'=>array(
                    'name',
                    'id',
                            array(
                        'label'=>'Category',
                        'type'=>'raw',
                        'value'=>$model->category->name,
                    ),
                            array(
                    'label'=>'Photo',
                    'type'=>'raw',
// If image exests- show image, else show no photo image
                    'value'=> $this->assortiment_image($model->id, $model->name),
            ),
                array(
                    'label'=>'description',
                    'type'=>'html',
                    'value'=>$model->description,
        ),
    ),
));  ?>