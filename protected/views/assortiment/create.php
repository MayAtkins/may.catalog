<?php
$this->breadcrumbs=array(
        'Assortiments'=>array('index'),
        'Create',
);

$this->menu=array(
        array('label'=>'Лист ассортиментов', 'url'=>array('index')),
        array('label'=>'Управление ассортиментов', 'url'=>array('admin')),
);
?>

<h1>Создание Ассортиментов</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>