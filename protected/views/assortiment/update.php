<?php
$this->breadcrumbs=array(
        'Assortiments'=>array('index'),
        $model->name=>array('view','id'=>$model->id),
        'Update',
);

$this->menu=array(
        array('label'=>'Лист ассортиментов', 'url'=>array('index')),
        array('label'=>'Создать ассортимент', 'url'=>array('create')),
        array('label'=>'Смотреть ассортимент', 'url'=>array('view', 'id'=>$model->id)),
        array('label'=>'Управление ассортиментов', 'url'=>array('admin')),
);
?>

<h1>Изменение ассортимента <?php echo $model->id; ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>