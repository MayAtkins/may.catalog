<?php
$this->breadcrumbs=array(
        'Assortiments',        
);

$this->menu=array(
        array('label'=>'Создать ассортименты', 'url'=>array('create')),
        array('label'=>'Управление ассортиментов', 'url'=>array('admin')),
);
?>


<h1>Ассортименты</h1>

<?php $this->widget('zii.widgets.CListView', array(
        'dataProvider'=>$dataProvider,
        'itemView'=>'_view',
)); ?>