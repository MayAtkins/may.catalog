<?php $form=$this->beginWidget('CActiveForm', array(
        'id'=>'assortiment-form',
        'enableAjaxValidation'=>false,
        'htmlOptions'=>array('enctype'=>'multipart/form-data'),
)); ?>
<div class="form">
      <? echo $this->assortiment_image($model->id, $model->name, '100','my') ?>
<?
//Если картинка для данного товара загружена, 
//предложить её удалить отметив чекбокс
if(file_exists(Yii::app()->urlManager->baseUrl.'/../assortiment_img/'.$model->id.'_assortiment.jpg'))
{
echo '<div class="row">';
echo $form->labelEx($model,'del_img'); 
echo $form->checkBox($model,'del_img' );   
echo '</div>';
 }
?> 
<? echo '<br />' ?>
<?php echo CHtml::activeFileField($model, 'icon'); ?>



        <p class="note">Fields with <span class="required">*</span> are required.</p>

        <?php echo $form->errorSummary($model); ?>

        <div class="row">
                <?php echo $form->labelEx($model,'name'); ?>
                <?php echo $form->textArea($model,'name',array('rows'=>1, 'cols'=>50)); ?>
                <?php echo $form->error($model,'name'); ?>
        </div>

        <div class="row">
                <?php echo $form->labelEx($model,'description'); ?>
                <?php echo $form->textArea($model,'description',array('rows'=>6, 'cols'=>50)); ?>
                <?php echo $form->error($model,'description'); ?>
        </div>

        <div class="row">
                <?php echo $form->labelEx($model,'rating'); ?>
                <?php echo $form->textField($model,'rating'); ?>
                <?php echo $form->error($model,'rating'); ?>
        </div>
        <div class="row">
                <?php echo $form->labelEx($model,'category_id'); ?>
                <?php echo $form->textField($model,'category_id');?>
                <?php echo $form->error($model,'category_id'); ?>
        </div>
                <?php
                  //  echo $form->dropDownList($model,'category_id', 
                    //        Category::All(),
                    //array('empty' => '(Select a category)')
                      //                  );?>   

        <div class="row">
                <?php echo $form->labelEx($model,'shown'); ?>
                <?php echo $form->textField($model,'shown');?>
                <?php echo $form->error($model,'shown'); ?>
        </div>

        <div class="row buttons">
                <?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
        </div>

<?php $this->endWidget(); ?>
    
<?php echo $form->error($model,'category_id'); ?>
</div><!-- form -->